#include <Adafruit_NeoPixel.h>

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // initialize the strip
  strip.begin();
  // initialize all pixels to 'off'
  strip.show();
  // brightness value is in a range from 0 to 255
  strip.setBrightness(255);
}

/**
 * This effect looks best with the LED strip vertical, and shows one or more bouncing balls in a given color.
 * This effect is an adapted version of bouncing balls by Danny Wilson.
 * The parameters are the color of all the balls, the last parameter is the number of balls you want to see bounce.
 */
void loop() {
  bouncingBalls(0xff, 0, 0, 3);
}

void bouncingBalls(byte red, byte green, byte blue, int ballCount) {
  float gravity = -9.81;
  int startHeight = 1;

  float height[ballCount];
  float impactVelocityStart = sqrt(-2 * gravity * startHeight);
  float impactVelocity[ballCount];
  float timeSinceLastBounce[ballCount];
  int position[ballCount];
  long clockTimeSinceLastBounce[ballCount];
  float dampening[ballCount];

  for (int i = 0; i < ballCount; i++) {
    clockTimeSinceLastBounce[i] = millis();
    height[i] = startHeight;
    position[i] = 0;
    impactVelocity[i] = impactVelocityStart;
    timeSinceLastBounce[i] = 0;
    dampening[i] = 0.90 - float(i) / pow(ballCount, 2);
  }

  while (true) {
    for (int i = 0; i < ballCount; i++) {
      timeSinceLastBounce[i] = millis() - clockTimeSinceLastBounce[i];
      height[i] = 0.5 * gravity * pow(timeSinceLastBounce[i] / 1000, 2.0) + impactVelocity[i] * timeSinceLastBounce[i] / 1000;

      if (height[i] < 0) {
        height[i] = 0;
        impactVelocity[i] = dampening[i] * impactVelocity[i];
        clockTimeSinceLastBounce[i] = millis();

        if (impactVelocity[i] < 0.01) {
          impactVelocity[i] = impactVelocityStart;
        }
      }
      position[i] = round(height[i] * (NUM_LEDS - 1) / startHeight);
    }

    for (int i = 0; i < ballCount; i++) {
      setPixel(position[i], red, green, blue);
    }

    showStrip();
    setAll(0, 0, 0);
  }
}

void showStrip() {
  strip.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  strip.setPixelColor(pixelNumber, strip.Color(red, green, blue));
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
