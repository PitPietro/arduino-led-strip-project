#include <Adafruit_NeoPixel.h>

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // initialize the strip
  strip.begin();
  // initialize all pixels to 'off'
  strip.show();
  // brightness value is in a range from 0 to 255
  strip.setBrightness(255);
}

/**
 * This effect makes multiple groups of LEDs chase each other.
 * It takes 4 parameters, of which the first 3 define the color.
 * The last parameter indicates how much delay is put in the loop, the higher the number, the slower it will go.
 */
void loop() {
  RunningLights(0xff, 0, 0, 50);        // red
  RunningLights(0xff, 0xff, 0xff, 50);  // white
  RunningLights(0, 0, 0xff, 50);        // blue
}

void RunningLights(byte red, byte green, byte blue, int waveDelay) {
  int position = 0;

  for (int j = 0; j < NUM_LEDS * 2; j++) {
    position++;  // = 0; //Position + Rate;
    for (int i = 0; i < NUM_LEDS; i++) {
      // sine wave, 3 offset waves make a rainbow!
      // float level = sin(i+Position) * 127 + 128;
      // setPixel(i,level,0,0);
      // float level = sin(i+Position) * 127 + 128;
      setPixel(i, ((sin(i + position) * 127 + 128) / 255) * red,
               ((sin(i + position) * 127 + 128) / 255) * green,
               ((sin(i + position) * 127 + 128) / 255) * blue);
    }

    showStrip();
    delay(waveDelay);
  }
}

void showStrip() {
  strip.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  strip.setPixelColor(pixelNumber, strip.Color(red, green, blue));
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
