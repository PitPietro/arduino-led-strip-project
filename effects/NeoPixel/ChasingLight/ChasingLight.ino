// Chasing Light
#include <Adafruit_NeoPixel.h>
// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

// increase this value to slow down the effect
unsigned short int delayTime = 100;

void setup() {
  // initialize the strip
  strip.begin();
  // initialize all pixels to 'off'
  strip.show();
  // brightness value is in a range from 0 to 255
  strip.setBrightness(255);
}

void loop() {
  forward(0x30, 0x20, 0xFF);
  downward(0x30, 0x20, 0xFF);
}

/**
 * Add '0x' in front of each of these hex values when using them.
 * '0x' designates a hexadecimal value, with which a color is composed.
 * Those bytes are ment to be hexadecimal values that compose the RBG LED parts.
*/
void forward(byte red, byte green, byte blue) {
  for(int i = 0; i < NUM_LEDS; i++) {
    setAll(0x00, 0x00, 0x00);
    setPixel(i-2, green, blue, red);
    setPixel(i-1, blue, red, green);
    setPixel(i, red, green, blue);
    showStrip();
    delay(delayTime);
  }
}

void downward(byte red, byte green, byte blue) {
  for(int i = NUM_LEDS-1; i >= 0; i--) {
    setAll(0x00, 0x00, 0x00);
    setPixel(i-2, green, blue, red);
    setPixel(i-1, blue, red, green);
    setPixel(i, red, green, blue);
    showStrip();
    delay(delayTime);
  }
}

void showStrip() {
  strip.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  strip.setPixelColor(pixelNumber, strip.Color(red, green, blue));
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
