#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

void loop() {
  Scanner(0xff, 0, 0, 4, 10, 50);
  Scanner(0xff, 0xaa, 0, 2, 20, 100);
  Scanner(0, 0xaa, 0xff, 10, 5, 15);
}

void Scanner(byte red, byte green, byte blue, int eyeSize, int speedDelay, int returnDelay) {
  for (int i = 0; i < NUM_LEDS - eyeSize - 2; i++) {
    setAll(0, 0, 0);
    setPixel(i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(i + j, red, green, blue);
    }
    setPixel(i + eyeSize + 1, red / 10, green / 10, blue / 10);
    showStrip();
    delay(speedDelay);
  }

  delay(returnDelay);

  for (int i = NUM_LEDS - eyeSize - 2; i > 0; i--) {
    setAll(0, 0, 0);
    setPixel(i, red / 10, green / 10, blue / 10);
    for (int j = 1; j <= eyeSize; j++) {
      setPixel(i + j, red, green, blue);
    }
    setPixel(i + eyeSize + 1, red / 10, green / 10, blue / 10);
    showStrip();
    delay(speedDelay);
  }

  delay(returnDelay);
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
