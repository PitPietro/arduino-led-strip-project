#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100
// brightness threshold
#define BRIGHT 255

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

/**
 * This effect makes multiple groups of LEDs chase each other.
 * It takes 4 parameters, of which the first 3 define the color.
 * The last parameter indicates how much delay is put in the loop, the higher the number, the slower it will go.
 */
void loop() {
  RunningLights(0xff, 0, 0, 50);        // red
  RunningLights(0xff, 0xff, 0xff, 50);  // white
  RunningLights(0, 0, 0xff, 50);        // blue
}

void RunningLights(byte red, byte green, byte blue, int waveDelay) {
  int position = 0;

  for (int j = 0; j < NUM_LEDS * 2; j++) {
    position++;  // = 0; //Position + Rate;
    for (int i = 0; i < NUM_LEDS; i++) {
      // sine wave, 3 offset waves make a rainbow!
      // float level = sin(i+Position) * 127 + 128;
      // setPixel(i,level,0,0);
      // float level = sin(i+Position) * 127 + 128;
      setPixel(i, ((sin(i + position) * 127 + 128) / 255) * red,
               ((sin(i + position) * 127 + 128) / 255) * green,
               ((sin(i + position) * 127 + 128) / 255) * blue);
    }

    showStrip();
    delay(waveDelay);
  }
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
