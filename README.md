# Arduino NeoPixel Project

## Documentation

- [The Magic of NeoPixels](https://learn.adafruit.com/adafruit-neopixel-uberguide)

### Products

- [Adafruit NeoPixel NeoMatrix 8x8 - 64 RGB LED Pixel Matrix](https://www.adafruit.com/product/1487)

